/* SnapRename - Snapshot Renamer for NetApp Data OnTap.
 * Copyright (c) 2013 Bytes Technology Group (Pty) Limited.
 * All Rights Reserved.
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <cassert>
#include "argvparser.h"
#include <windows.h>
using namespace std;
using namespace CommandLineProcessing;

void writeToFile(string line, string filename) {
    ofstream fout(filename.c_str());
    fout << line;
    fout.close();
}

string readFromFile(string filename) {
    ifstream fin(filename.c_str());
    string line = "";
    getline(fin, line, '\n');
    fin.close();
    return line;
}

void error(string input) {
    cerr << input << endl;
    exit(1);
}

string getSnapShotName(const string filename) {
    ifstream fin(filename.c_str());
    if (fin.bad())    error("ERROR: Unable to open file: " + filename);
    string line = "";
    unsigned int count = 0;
    const unsigned int CLIMIT = 8;
    while (!fin.eof() && !fin.bad() && count < CLIMIT) {
        fin >> line;
        count++;
        //cout << line << endl;
    }
    fin.close();
    if (count < CLIMIT) error("ERROR: Unable to find last snapshot.  Aborting.");
    return line;
}

void doArgs(int argc, char *argv[], ArgvParser &cmd) {
    string intro = "SnapRename v1.0 - Snapshot Renamer for NetApp Data OnTap.";
    string copyright = "Copyright (c) 2013 Bytes Technology Group (Pty) Limited.\nAll Rights Reserved.";
    cmd.setIntroductoryDescription(intro + "\n" + copyright);
    cmd.setHelpOption("h", "help", "Renames Snapshots.");
    cmd.defineOption("filer", "Hostname of filer", ArgvParser::OptionRequired | ArgvParser::OptionRequiresValue);
    cmd.defineOption("vol", "Volume name", ArgvParser::OptionRequired | ArgvParser::OptionRequiresValue);
    cmd.defineOption("renameto", "Rename snapshot to this", ArgvParser::OptionRequiresValue | ArgvParser::OptionRequired);
    cmd.defineOption("pre", "Pre-backup: Renames latest snapshot to known name");
    cmd.defineOption("post", "Post-backup: Renames known name back to original name");
    cmd.defineOption("username" ,"Username for filer access", ArgvParser::OptionRequired | ArgvParser::OptionRequiresValue);
    cmd.defineOption("password", "Password for filer access", ArgvParser::OptionRequired | ArgvParser::OptionRequiresValue);
    int result = cmd.parse(argc, argv);
    if (result != ArgvParser::NoParserError) {
        cout << cmd.parseErrorDescription(result) << endl;
        exit(1);
    }
    cout << intro << endl << copyright << endl;
}

void runProcess(ArgvParser &cmd) {
    string filer = cmd.optionValue("filer");
    string vol = cmd.optionValue("vol");
    string username = cmd.optionValue("username");
    string password = cmd.optionValue("password");
    string renameto = "";
    int mode = 0;
    const int PREMODE = 1;
    const int POSTMODE = 2;
    if (cmd.foundOption("pre"))   mode = PREMODE;
    if (cmd.foundOption("post"))  mode = POSTMODE;
    if (mode == 0)  error("ERROR: Select either PRE or POST mode of operation.");
    renameto = cmd.optionValue("renameto");

    string tempFile = "snaptempinfo.txt";
    string snapPers = "snappers" + vol + ".txt";
    string lastsnap = "";
    if (mode == PREMODE) {
        string pl = "plink -ssh -l " + username + " -pw " + password + " " + filer + " -batch snap list -b " + vol + " > " + tempFile;
        cout << pl << endl;

        system(pl.c_str());
        lastsnap = getSnapShotName(tempFile);
        cout << "Rename " << lastsnap << " to " << renameto << "." << endl;
        string renamecmd = "plink -ssh -l " + username + " -pw " + password + " " + filer + " -batch snap rename " + vol + " " + lastsnap + " " + renameto;
        cout << renamecmd << endl;
        system(renamecmd.c_str());
        writeToFile(lastsnap, snapPers);
        remove(tempFile.c_str());
    } else if (mode == POSTMODE) {
        lastsnap = readFromFile(snapPers);
        //assert(lastsnap.length() != 0);
        if (lastsnap.length() == 0) error("ERROR: Unable to determine original snapshot name.  Aborting.");
        //cout << "lastsnap is \"" << lastsnap << "\"" << endl;
        cout << "Rename " << renameto << " back to " << lastsnap << "." << endl;
        string renamecmd = "plink -ssh -l " + username + " -pw " + password + " " + filer + " -batch snap rename " + vol + " " + renameto + " " + lastsnap;
        cout << renamecmd << endl;
        system(renamecmd.c_str());
        remove(snapPers.c_str());
    }
}

int main(int argc, char *argv[])
{
    ArgvParser cmd;
    doArgs(argc, argv, cmd);
    runProcess(cmd);
    const string filename = "c:\\projects\\snaplist.txt";
    //cout << "The snapshot is called \"" << getSnapShotName(filename) << "\"" << endl;
    return 0;
}
